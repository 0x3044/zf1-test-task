insert into product
  (id, name, description, price, amount)
VALUES
  (1, 'Apple', 'Some apples', 1000, 120),
  (2, 'Cabbage', 'Some cabbages', 1200, 8),
  (3, 'Cherry', 'Some cabbages', 2400, 30),
  (4, 'Chestnut', 'Some cabbages', 800, 40),
  (5, 'Corn', 'Some cabbages', 1000, 16),
  (6, 'Pear', 'Some cabbages', 1400, 200),
  (7, 'Pecan', 'Some cabbages', 1600, 12),
  (8, 'Plum', 'Some cabbages', 800, 80),
  (9, 'Potato', 'Some cabbages', 300, 400),
  (10, 'Pumpkin', 'Some cabbages', 1800, 10),
  (11, 'Squash', 'Some cabbages', 900, 40),
  (12, 'Walnut', 'Some cabbages', 1100, 5),
  (13, 'Wheat', 'Some cabbages', 200, 900),
  (14, 'Wood', 'Some cabbages', 700, 15),
  (15, 'Stone', 'Some cabbages', 800, 10),
  (16, 'Herbs', 'Some cabbages', 900, 10),
  (17, 'Gold', 'Some cabbages', 120000, 5),
  (18, 'Tools', 'Some cabbages', 40000, 28)