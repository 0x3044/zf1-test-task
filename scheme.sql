CREATE TABLE IF NOT EXISTS product (
  id int auto_increment primary key,
  name tinytext,
  description text,
  preview tinytext,
  price int,
  amount int
);

CREATE TABLE IF NOT EXISTS orders (
  id int auto_increment primary key
);

CREATE TABLE IF NOT EXISTS products_block (
  id INT AUTO_INCREMENT PRIMARY KEY,
  order_id INT,
  product_id INT,
  date_created DATETIME,
  date_updated DATETIME,
  amount INT,
  FOREIGN KEY (order_id) REFERENCES orders(id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS orders_list (
    id INT AUTO_INCREMENT PRIMARY KEY,
    order_id INT,
    block_id INT,
    FOREIGN KEY (order_id) REFERENCES orders(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (block_id) REFERENCES products_block(id) ON DELETE CASCADE ON UPDATE CASCADE
);
