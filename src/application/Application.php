<?php
/** Zend_Application */
require_once 'Zend/Application.php';

class Shop_Application extends Zend_Application
{
    protected function _loadConfig($file)
    {
        Zend_Registry::set('config', $config = parent::_loadConfig($file));

        return $config;
    }
}