<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initAutoLoader()
    {
        $loader = Zend_Loader_Autoloader::getInstance();
        $loader->registerNamespace('Lib_');
    }

    protected function _initDbExceptions()
    {
        require_once realpath(__DIR__).'/models/Db/Exceptions.php';
    }

    protected function _initMemcached()
    {
        $config = Zend_Registry::get('config');

        Zend_Registry::set('memcached', Zend_Cache::factory('Core', 'Memcached', $config['memcached']['frontend'], $config['memcached']['backend']));
    }

    protected function _initOrdersService()
    {
        Zend_Registry::set('orders', new Application_Model_Order_Service());
    }
}