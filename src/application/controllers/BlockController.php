<?php
class BlockController extends Zend_Controller_Action
{
    /**
     * Clear order's cache
     */
    public function init()
    {
        parent::init();

        /** @var $ordersService Application_Model_Order_Service */
        $ordersService = Zend_Registry::get('orders');
        $ordersService->getCache()->resetCache();
    }

    /**
     * Create a block for some amount of product and bind this block to users current order
     */
    public function blockAction()
    {
        $success = $error = null;

        try {
            $blockService = new Application_Model_Block_Service();
            $blockService->blockProduct(
                (int) $this->_getParam('product_id'),
                (int) $this->_getParam('request_amount')
            );

            $success = true;
        }catch(Application_Model_Order_BlockService_NotAvailableException $e) {
            $success = false;
            $error = $e->getMessage();
        }catch(Application_Model_Order_BlockService_TooMuchRequiredException $e) {
            $success = false;
            $error = $e->getMessage();
        }

        $this->view->assign(array(
            'success' => $success,
            'error' => $error
        ));
    }

    /**
     * Destroy block by products_block.id
     */
    public function unblockAction()
    {
        $blockService = new Application_Model_Block_Service();
        $blockService->unblockProduct($this->_getParam('block_id'));

        $this->view->assign(array(
            'success' => true,
            'error' => null
        ));
    }
}