<?php

class OrdersController extends Zend_Controller_Action
{
    /**
     * Orders item list of current user
     */
    public function indexAction()
    {
        /** @var $ordersService Application_Model_Order_Service */
        $ordersService = Zend_Registry::get('orders');

        $this->view->assign(array(
            'orders' => $ordersService->getCurrentOrderList(),
            'loadedFromMemcached' => $ordersService->isLoadedFromMemcached()
        ));
    }
}