<?php
use Application_Model_Product_Repository as ProductRepository;

class ProductsController extends Zend_Controller_Action
{
    /**
     * Products list
     */
    public function indexAction()
    {
        $page = (int) $this->_getParam('page');
        $mapper = new ProductRepository();
        $records = $mapper->fetchAll($page);

        $data = range(1, ceil($mapper->count() / ProductRepository::NUM_DEFAULT_PRODUCTS_PER_PAGE));
        $paginator = Zend_Paginator::factory($data);

        $this->view->assign(array(
            'products' => $records,
            'page' => $page,
            'paginator' => $paginator
        ));
    }

    /**
     * Products details page
     */
    public function viewAction()
    {
        $id = (int) $this->_getParam('id');
        $mapper = new ProductRepository();

        $this->view->assign(array(
            'product' => $mapper->find($id)
        ));
    }
}