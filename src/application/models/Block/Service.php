<?php

/**
 * Blocking/unblocking product service
 */
class Application_Model_Block_Service
{
    const BLOCK_MINUTES_TO_EXPIRE = 30;

    /**
     * Table gateway for products_block
     * @var Application_Model_Db_ProductsBlock_Table
     */
    private $_blockTable;

    /**
     * Table gateway for orders_list
     * @var Application_Model_Db_OrdersList_Table
     */
    private $_orderListTable;

    /**
     * Block service
     */
    public function __construct()
    {
        $this->_blockTable = new Application_Model_Db_ProductsBlock_Table();
        $this->_orderListTable = new Application_Model_Db_OrdersList_Table();
    }

    /**
     * Returns session helper
     * @return Application_Model_Order_Session
     */
    protected function _getOrderSession()
    {
        /** @var $ordersService Application_Model_Order_Service */
        $ordersService = Zend_Registry::get('orders');

        return $ordersService->getSession();
    }

    /**
     * Returns table gateway for products_block table
     * @return Application_Model_Db_ProductsBlock_Table
     */
    protected function _getBlockTable()
    {
        return $this->_blockTable;
    }

    /**
     * Returns table gateway for orders_list table
     * @return Application_Model_Db_OrdersList_Table
     */
    protected function _getOrderListTable()
    {
        return $this->_orderListTable;
    }

    /**
     * Block some amount of product
     * Throw an exception if user cant block requested amount of product
     * @param $productId
     * @param $amount
     * @throws Application_Model_Order_BlockService_NotAvailableException
     * @throws Application_Model_Order_BlockService_TooMuchRequiredException
     */
    public function blockProduct($productId, $amount)
    {
        $repository = new Application_Model_Product_Repository();
        $product = $repository->find($productId);

        if($product->getActualAmount() <= 0) {
            throw new Application_Model_Order_BlockService_NotAvailableException("Product out of stock");
        }

        if($product->getActualAmount() < $amount) {
            throw new Application_Model_Order_BlockService_TooMuchRequiredException(sprintf('Only %d of %s\'s are available on stock', $product->getActualAmount(), $product->name));
        }

        $orderId = $this->_getOrderSession()->getCurrentOrderId();
        $blockId = $this->_getBlockTable()->blockProduct($productId, $orderId, $amount);
        $this->_getOrderListTable()->linkBlockToOrder($blockId, $orderId);
    }

    /**
     * Destroy product block by products_block.id
     * Also MySql will automatically destroy records from orders_list due ON CASCADE DELETE behaviour
     * @param $blockId
     * @return int
     */
    public function unblockProduct($blockId)
    {
        return $this->_getBlockTable()->destroyBlock($blockId);
    }

    /**
     * Clean up unused products blocks
     */
    public function cleanUpBlocks()
    {
        $minutesToExpire = self::BLOCK_MINUTES_TO_EXPIRE;

        $sqlQuery = <<<SQL
          DELETE FROM
            products_block
          WHERE TIME_TO_SEC(TIMEDIFF(NOW(),date_updated)) / 60 < {$minutesToExpire}
SQL;

        Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery);
    }
}

class Application_Model_Order_BlockService_NotAvailableException extends \Exception
{
}

class Application_Model_Order_BlockService_TooMuchRequiredException extends \Exception
{
}