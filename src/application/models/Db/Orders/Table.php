<?php
class Application_Model_Db_Orders_Table extends Zend_Db_Table_Abstract
{
    protected $_name = 'orders';
    protected $_primary = 'id';

    /**
     * Create an order
     * @return int
     */
    public function createOrder()
    {
        return (int) $this->insert(array());
    }
}