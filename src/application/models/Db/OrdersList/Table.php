<?php
class Application_Model_Db_OrdersList_Table extends Zend_Db_Table_Abstract
{
    protected $_name = 'orders_list';
    protected $_primary = 'id';

    /**
     * Link products block to the order
     * @param $blockId
     * @param $orderId
     * @return mixed
     * @throws Exception
     */
    public function linkBlockToOrder($blockId, $orderId)
    {
        $idValidator = new Lib_Validate_Id();

        if(!($idValidator->isValid($blockId))) {
            throw new \Exception(sprintf('Invalid blockId `%s`', var_export($blockId)));
        }

        if(!($idValidator->isValid($orderId))) {
            throw new \Exception(sprintf('Invalid orderId `%s`', var_export($orderId)));
        }

        return $this->insert(array(
            'block_id' => $blockId,
            'order_id' => $orderId
        ));
    }
}