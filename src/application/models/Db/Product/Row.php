<?php

class Application_Model_Db_Product_Row extends Zend_Db_Table_Row_Abstract
{
    /**
     * Return URL to the product details page
     * @return string
     */
    public function getUrl()
    {
        return sprintf('/products/view/?id=%d', $this->id);
    }

    /**
     * Return price as decimal
     * @return string
     */
    public function getPriceAsDecimal()
    {
        $money = new Lib_Money((int) $this->price);

        return $money->toDecimal();
    }

    /**
     * Return actual products based on existing products blocks
     * @return int
     */
    public function getActualAmount()
    {
        return max(0, (int) $this->amount - (int) $this->blockedAmount);
    }
}