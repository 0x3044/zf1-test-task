<?php
use Application_Model_NotFoundException as NotFoundException;

class Application_Model_Db_Product_Table extends Zend_Db_Table_Abstract
{
    const DEFAULT_NUM_PRODUCTS_PER_PAGE = 20;

    protected $_name = 'product';
    protected $_primary = 'id';
    protected $_rowClass = 'Application_Model_Db_Product_Row';

    /**
     * Return product by id
     * @param $id
     * @param bool $blockDetails
     * @throws Application_Model_NotFoundException
     * @throws Exception
     * @return null|Application_Model_Db_Product_Row
     */
    public function find($id, $blockDetails = true)
    {
        $validator = new Lib_Validate_Id();

        if(!($validator->isValid($id))) {
            throw new \Exception(sprintf('Invalid id, expected int(>0), got %s', var_export($id, true)));
        }

        $select = $this->select()->where('product.id = ?', $id);

        if($blockDetails) {
            $this->_addBlockConditions($select);
        }

        if(!($result = $this->fetchRow($select))) {
            throw new NotFoundException(sprintf('Product with id `%d` not found', $id));
        }

        return $result;
    }

    /**
     * Return list of products
     * @param array $options Options [allResultsAllowed, page, perPage]
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws Exception
     */
    public function listProducts(array $options = array())
    {
        $options = array_merge(array(
            'allResultsAllowed' => false,
            'page' => null,
            'perPage' => self::DEFAULT_NUM_PRODUCTS_PER_PAGE,
            'blockDetails' => true
        ), $options);

        $positiveNumericValidator = new Zend_Validate();
        $positiveNumericValidator->addValidator(new Zend_Validate_Int())->addValidator(new Zend_Validate_GreaterThan(-1));

        // validate options
        foreach($options as $option => &$value) {
            switch($option) {
                case 'page':
                    if(!(is_null($value))) {
                        if(!($positiveNumericValidator->isValid($value))) {
                            throw new \Exception(sprintf("Invalid `page` value `%s`", var_export($value, true)));
                        }
                    }

                    break;

                case 'perPage':
                    if(!($positiveNumericValidator->isValid($value))) {
                        throw new \Exception(sprintf("Invalid `perPage` value `%s`", var_export($value, true)));
                    }
                    break;

                case 'allResultsAllowed':
                    if(!is_bool($value)) {
                        throw new \Exception(sprintf("Invalid `allResultsAllowed` value `%s`", var_export($value, true)));
                    }
            }
        }

        // build where query
        $numConstraints = 0;
        $select = $this->select()->order('id asc');

        if(!(is_null($options['page']))) {
            $numConstraints++;
            $select->limitPage($options['page'], $options['perPage']);
        }

        // security: DO NOT allow methods get full list of products for no reason no matter how it became possible
        if(!($options['allResultsAllowed']) && $numConstraints == 0 ) {
            throw new Application_Model_SecurityException('No where conditions has been set; use allResultsAllowed to enable result');
        }

        // Append block details
        if($options['blockDetails']) {
            $this->_addBlockConditions($select);
        }

        return $this->fetchAll($select);
    }

    /**
     * Add actual amount information to the query
     * @param Zend_Db_Table_Select $select
     */
    protected function _addBlockConditions(Zend_Db_Table_Select $select)
    {
        $select->setIntegrityCheck(false)
            ->from('product')
            ->joinLeft('products_block', 'product.id = products_block.product_id', array('blockedAmount' => new Zend_Db_Expr('SUM(products_block.amount)')))
            ->group('product.id')
        ;
    }
}