<?php

class Application_Model_Db_ProductsBlock_Table extends Zend_Db_Table_Abstract
{
    protected $_name = 'products_block';
    protected $_primary = 'id';

    /**
     * Create products block
     * @param $productId
     * @param $orderId
     * @param $amount
     * @return mixed
     * @throws Exception
     */
    public function blockProduct($productId, $orderId, $amount)
    {
        $idValidator = new Lib_Validate_Id();

        if(!($idValidator->isValid($orderId))) {
            throw new \Exception(sprintf('Invalid orderId `%s`', var_export($orderId, true)));
        }

        if(!($idValidator->isValid($productId))) {
            throw new \Exception(sprintf('Invalid productId `%s`', var_export($productId, true)));
        }

        $amountValidator = new Zend_Validate();
        $amountValidator->addValidator(new Zend_Validate_Int())->addValidator(new Zend_Validate_GreaterThan(0));

        if(!($amountValidator->isValid($amount))) {
            throw new \Exception(sprintf('Invalid amount `%s`', var_export($amount, true)));
        }

        return $this->insert(array(
            'order_id' => (int) $orderId,
            'product_id' => (int) $productId,
            'date_created' => new Zend_Db_Expr('NOW()'),
            'date_updated' => new Zend_Db_Expr('NOW()'),
            'amount' => (int) $amount
        ));
    }

    /**
     * Destroy products block by id
     * @param $blockId
     * @return int
     * @throws Exception
     */
    public function destroyBlock($blockId)
    {
        $idValidator = new Lib_Validate_Id();

        if(!($idValidator->isValid($blockId))) {
            throw new \Exception(sprintf('Invalid blockId `%s`', var_export($blockId, true)));
        }

        return $this->delete(array('id = ?' => $blockId));
    }

    /**
     * Update, "touch" blocks by orderId
     * @param $orderId
     * @return int
     * @throws Exception
     */
    public function touchBlocks($orderId)
    {
        $idValidator = new Lib_Validate_Id();

        if(!($idValidator->isValid($orderId))) {
            throw new \Exception(sprintf('Invalid orderId `%s`', var_export($orderId, true)));
        }

        return $this->update(array(
            'date_updated' => new Zend_Db_Expr('NOW()'),
        ), array('order_id = ?' => $orderId));
    }
}