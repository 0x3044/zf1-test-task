<?php

/**
 * Caching helper for orders
 */
class Application_Model_Order_Cache
{
    const MEMCACHED_LIST_ORDERS_KEY = 'orders_list_%d';

    /**
     * Returns memcached handler
     * @return Zend_Cache_Core
     */
    protected function _getCache()
    {
        return Zend_Registry::get('memcached');
    }

    /**
     * Return memcached key based on current orders id
     * @return string
     * @throws Exception
     */
    protected function _getMemcachedListOrdersKey()
    {
        /** @var $ordersService Application_Model_Order_Service */
        $ordersService = Zend_Registry::get('orders');

        if(!($ordersService->hasOrder())) {
            throw new \Exception('OrderId is not available');
        }

        return sprintf(self::MEMCACHED_LIST_ORDERS_KEY, $ordersService->getCurrentOrderId());
    }

    /**
     * Reset orders list cache
     * Use this method when you do any changes in orders list
     */
    public function resetCache()
    {
        /** @var $ordersService Application_Model_Order_Service */
        $ordersService = Zend_Registry::get('orders');

        if($ordersService->hasOrder()) {
            $cache = $this->_getCache();
            $cache->remove($this->_getMemcachedListOrdersKey());
        }
    }

    /**
     * Returns cached data of orders list
     * @return false|mixed
     */
    public function getCachedOrders()
    {
        $cache = $this->_getCache();
        return $cache->load($this->_getMemcachedListOrdersKey());
    }

    /**
     * Setup and fill cache for orders list
     * @param $data
     */
    public function setCachedOrders($data)
    {
        $cache = $this->_getCache();
        $cache->save($data, $this->_getMemcachedListOrdersKey());
    }

    /**
     * Return true if memcached have some cached data
     * @return bool
     */
    public function hasCachedOrders()
    {
        return is_array($this->getCachedOrders());
    }
}