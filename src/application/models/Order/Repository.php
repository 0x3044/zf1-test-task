<?php
use Application_Model_Db_OrdersList_Table as OrderListTable;

class Application_Model_Order_Repository
{
    /**
     * Returns full list of orders product blocks
     * @param $orderId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function fetchAll($orderId)
    {
        $orderListTable = new OrderListTable();

        $select = $orderListTable->select()
            ->setIntegrityCheck(false)
            ->from('orders_list', '*')
            ->joinLeft('products_block', 'orders_list.block_id = products_block.id', array(
                'date_created' => 'products_block.date_created',
                'date_updated' => 'products_block.date_updated',
                'amount' => 'products_block.amount',
            ))
            ->joinLeft('product', 'products_block.product_id = product.id', array(
                'price' => 'product.price',
                'product_name' => 'product.name',
            ))
            ->where('orders_list.order_id = ?', $orderId)
        ;

        return $orderListTable->fetchAll($select);
    }
}