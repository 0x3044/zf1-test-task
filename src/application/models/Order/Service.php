<?php
use Application_Model_Order_Cache as OrdersCache;
use Application_Model_Order_Repository as OrdersRepository;
use Application_Model_Order_Session as OrdersSession;

class Application_Model_Order_Service
{
    /**
     * Cache helper
     * @var OrdersCache
     */
    protected $_cache;

    /**
     * Orders repository
     * @var OrdersRepository
     */
    protected $_repository;

    /**
     * Session helper
     * @var OrdersSession
     */
    protected $_session;

    /**
     * Flag "data was loaded from cache"
     * @var bool
     */
    protected $_loadedFromMemcached = false;

    /**
     * Returns cache helper
     * @return Application_Model_Order_Cache
     */
    public function getCache() {
        if(is_null($this->_cache)) {
            $this->_cache = new OrdersCache();
        }

        return $this->_cache;
    }

    /**
     * Returns orders repository
     * @return Application_Model_Order_Repository
     */
    public function getRepository() {
        if(is_null($this->_repository)) {
            $this->_repository = new OrdersRepository();
        }

        return $this->_repository;
    }

    /**
     * Returns session helper
     * @return Application_Model_Order_Session
     */
    public function getSession() {
        if(is_null($this->_session)) {
            $this->_session = new OrdersSession();
        }

        return $this->_session;
    }

    /**
     * Returns true if application had created order for current user
     * @return bool
     */
    public function hasOrder() {
        return $this->getSession()->hasOrderId();
    }

    /**
     * Returns id of current order
     * @return int|mixed
     */
    public function getCurrentOrderId() {
        return $this->getSession()->getCurrentOrderId();
    }

    /**
     * Return items count in the orders list
     * @return int
     */
    public function getCurrentOrdersListCount() {
        return count($this->getCurrentOrderList());
    }

    /**
     * Return orders list
     * @return array|false|mixed
     */
    public function getCurrentOrderList() {
        $orders = array();
        $session = $this->getSession();

        if($session->hasOrderId()) {
            $cache = $this->getCache();


            if($loadedFromMemcached = $cache->hasCachedOrders()) {
                $orders = $cache->getCachedOrders();

                $this->_loadedFromMemcached = true;
            }else{
                $ordersMapper = new Application_Model_Order_Repository();
                $orders = $ordersMapper->fetchAll($this->getCurrentOrderId())->toArray();
                $cache->setCachedOrders($orders);

                $this->_loadedFromMemcached = false;
            }
        }

        return $orders;
    }

    /**
     * Return true if orders was loaded from memcached
     * @return boolean
     */
    public function isLoadedFromMemcached()
    {
        return $this->_loadedFromMemcached;
    }
}