<?php
class Application_Model_Order_Session
{
    const SESSION_KEY_ORDER_ID = 'order_id';

    /**
     * OrderId for order of current user
     * @var int
     */
    protected $_currentOrderId;

    /**
     * Returns true if application had created order for current user
     * @return bool
     */
    public function hasOrderId()
    {
        $zendOrderNamespace = new Zend_Session_Namespace('Order');

        return $this->_currentOrderId > 0 || (int) $zendOrderNamespace->id > 0;
    }

    /**
     * Returns orderId of current order
     * If order doesnt exists method will create order automatically
     * @return int|mixed
     */
    public function getCurrentOrderId()
    {
        $zendOrderNamespace = new Zend_Session_Namespace('Order');

        if(is_null($this->_currentOrderId)) {
            if((int) $zendOrderNamespace->id > 0) {
                $this->_currentOrderId = $zendOrderNamespace->id;
            }else{
                $newOrderId = $this->_createNewOrder();

                $zendOrderNamespace->id = $newOrderId;
                $this->_currentOrderId = $newOrderId;
            }
        }

        return $this->_currentOrderId;
    }

    /**
     * Create new order
     * @return int
     */
    protected function _createNewOrder()
    {
        $orderTable = new Application_Model_Db_Orders_Table();

        return $orderTable->createOrder();
    }
}