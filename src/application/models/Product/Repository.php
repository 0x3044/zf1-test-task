<?php
use Application_Model_Db_Product_Table as ProductTable;

class Application_Model_Product_Repository
{
    const NUM_DEFAULT_PRODUCTS_PER_PAGE = 5;

    /**
     * @var ProductTable
     */
    protected $_table;

    /**
     * Return products table
     * @return Application_Model_Db_Product_Table
     */
    public function _getTable() {
        if(is_null($this->_table)) {
            $this->_table = new ProductTable();
        }

        return $this->_table;
    }

    /**
     * Returns product by id
     * @param $id
     * @return Application_Model_Db_Product_Row|null
     */
    public function find($id)
    {
        return $this->_getTable()->find($id);
    }

    /**
     * Returns list of products
     * @param $page
     * @param null $perPage
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function fetchAll($page, $perPage = null)
    {
        if(is_null($perPage)) {
            $perPage = self::NUM_DEFAULT_PRODUCTS_PER_PAGE;
        }

        return $this->_getTable()->listProducts(array(
            'page' => $page,
            'perPage' => $perPage
        ));
    }

    /**
     * Returns count of all products
     * @return int
     */
    public function count()
    {
        $table = $this->_getTable();
        $query = $table->select()->from($table, array('num' => 'COUNT(*)'));
        $result = $table->fetchRow($query);

        return (int) $result->num;
    }
}