<?php
/**
 * ZF2 has a console support for runnian actions from controllers out of the box
 * ZF1 hasn't.
 *
 * At current ERP project I'm using phpunit for running some migrations and tools, but ofc we can type "zf1 console
 * command action" in google and copy-paste one of solutions from stack-overflow.
 */

require_once realpath(__DIR__).'/../public/index.php';

