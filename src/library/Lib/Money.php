<?php
/**
 * Yes, I should realize money pattern
 * Oh wait, I have no time
 */
class Lib_Money
{
    protected $_value;

    public function __construct($value)
    {
        $validateChain = new Zend_Validate();
        $validateChain->addValidator(new Zend_Validate_Alnum());
        $validateChain->addValidator(new Zend_Validate_Int());

        if(!($validateChain->isValid($value))) {
            throw new \Exception(sprintf('Invalid money value `%s`', var_export($value, true)));
        }

        $this->_value = $value;
    }

    public function toDecimal()
    {
        return money_format('%i', ($this->_value / 100));
    }
}