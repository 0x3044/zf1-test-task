<?php
class Tests_Application_Models_Db_ProductsTableTest extends PHPUnit_Framework_TestCase
{
    protected $_fixture = array(
        ['id' => 1, 'name' => 'Apple', 'description' => 'Horo will be happy!', 'preview' => '1.png', 'price' => 150, 'amount' => 3000],
        ['id' => 2, 'name' => 'Infinity Edge', 'description' => 'x1.5 critical damage', 'preview' => '2.png', 'price' => 380000, 'amount' => 2],
        ['id' => 3, 'name' => 'Primordial Saronite', 'description' => 'Crafting Reagent', 'preview' => '3.png', 'price' => 700, 'amount' => 12],
    );

    protected function setUp()
    {
        parent::setUp();

        $table = $this->_getTable();
        $table->delete('1 = 1');

        foreach($this->_fixture as $record) {
            $table->insert($record);
        }
    }

    protected function _getTable() {
        return new Application_Model_Db_Product_Table();
    }

    /**
     * Trying to get available product
     */
    public function testFindSuccess()
    {
        $table = $this->_getTable();
        $record = $table->find(1, false);

        $this->assertTrue($record instanceof Application_Model_Db_Product_Row);
        $this->assertEquals((int) $record->id, 1);
        $this->assertEquals($record->name, 'Apple');
    }

    /**
     * Trying to get unavailable product and catching exception
     */
    public function testFindFail()
    {
        $this->setExpectedException('Application_Model_NotFoundException');

        $table = $this->_getTable();
        $record = $table->find(999, false);
    }

    /**
     * No page limits; see all products
     */
    public function testListFull()
    {
        $table = $this->_getTable();

        $result = $table->listProducts(array(
            'allResultsAllowed' => true,
            'blockDetails' => false
        ));
        $compare = array();

        /** @var $row Zend_Db_Table_Row */
        foreach($result as $row) {
            $compare[] = $row->toArray();
        }

        $this->assertEquals($this->_fixture, $compare);
    }

    /**
     * Page 1: page => 1, perPage => 2, see first 2 products in the list
     */
    public function testListPage1()
    {
        $table = $this->_getTable();

        $result = $table->listProducts(array(
            'page' => 1,
            'perPage' => 2,
            'blockDetails' => false
        ));
        $compare = array();

        /** @var $row Zend_Db_Table_Row */
        foreach($result as $row) {
            $compare[] = $row->toArray();
        }

        $this->assertEquals(array_slice($this->_fixture, 0, 2), $compare);
    }

    /**
     * Page 2: page => 2, perPage => 2, see last product in the list
     */
    public function testListPage2()
    {
        $table = $this->_getTable();

        $result = $table->listProducts(array(
            'page' => 2,
            'perPage' => 2,
            'blockDetails' => false
        ));
        $compare = array();

        /** @var $row Zend_Db_Table_Row */
        foreach($result as $row) {
            $compare[] = $row->toArray();
        }

        $this->assertEquals(array_slice($this->_fixture, 2, 1), $compare);
    }

    /**
     * Security: trying to get list of all products and getting exception
     */
    public function testListNotAvailableFullResults()
    {
        $this->setExpectedException('Application_Model_SecurityException');

        $this->_getTable()->listProducts();
    }

    /**
     * Security: trying to get list of all products and NOT getting exception because we enabled full results
     */
    public function testListAvailableFullResults()
    {
        $this->_getTable()->listProducts(array(
            'allResultsAllowed' => true,
            'blockDetails' => false
        ));
    }
}